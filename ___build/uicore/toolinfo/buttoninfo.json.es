{
"@context": {
"uicore":"http://iridl.ldeo.columbia.edu/uicore/",
"term": "http://iridl.ldeo.columbia.edu/ontologies/iriterms.owl#",
"uicore:buttonList": {"@container": "@list"},
"term:label": {"@language": "es"},
"term:description": {"@language": "es"}
},
"uicore:lang": "es",
"uicore:buttonList": [
{
"uicore:icon": "zoomout",
"term:label": "Alejarse",
"term:description": "Restaura la visualización por defecto al cancelar las solicitudes de acercamiento anteriores. Note que el botón atrás cancela al acercamiento mas reciente en los navegadores modernos."
},
{
"uicore:icon": "info",
"term:label": "Mas Información",
"term:description": "Abre una página en la data library con más detalles y opciones sobre la imagen visualizada."
},
{
"uicore:icon": "settings",
"term:label": "Variables Independientes",
"term:description": "Controla la configuración de las variables independientes como el tiempo o la altura. Aparece solamente cuando hay opciones."
},
{
"uicore:icon": "layers",
"term:label": "Capas",
"term:description": "Activa o desactiva las capas en imágenes que tienen más de una capa de información. Aparece solamente cuando hay opciones."
},
{
"uicore:icon": "share",
"term:label": "Compartir",
"term:description": "Permite compartir la imagen visualizada con diferentes herramientas online y sitios de redes sociales. Las opciones para herramientas de cartografía no están disponibles cuando la imagen no es une mapa."
},
{
"uicore:icon": "download",
"term:label": "Bajar",
"term:description": "Permite bajar la imagen visualizada en diferentes formatos y protocoles. El nivel de información depende del formato: sólo KML, WMS y PDF incluyen un enlace hacia esta página."
},
{
"uicore:icon": "downloadselect",
"term:label": "Secciones con varias imágenes",
"term:description": "Indica con un cuadro negro la imagen seleccionada para compartir o bajar. Haga click sobre otra imagen para cambiar la selección."
},
{
"uicore:icon": "dlimageswitch",
"term:label": "Controles Bloqueados",
"term:description": "Al pulsar este botón, los controles permanecen visibles; de lo contrario, los controles aparecen y el botón está parcialmente pulsado, automáticamente, al activar la imagen."
}


]
}
